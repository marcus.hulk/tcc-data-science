### Especialização em Data Science e Analytics USP/Esalq##

### TCC Marcus Rocha ## 

# Check, load and install packages if necessary
 
check_packs <- function(packages) {
# Check if packages are installed and install if not
  for (package in packages) {
    if (!requireNamespace(package, quietly = TRUE)) {
      install.packages(package, dependencies = TRUE)
    }
  }
  
# Load 
  lapply(packages, library, character.only = TRUE)
}

# List of packages 
packages <- c(
  "geobr", "sidrar", "ggplot2", "ggmap", "RColorBrewer",
  "readr", "viridis", "colorspace", "scales", "ggthemes",
  "curl", "zip", "rio", "dplyr", "magrittr", "scales",
  "kableExtra", "sparkline", "modelsummary", "flextable", 
  "hrbrthemes", "lme4", "sjPlot", "lme4", "lmerTest", 
  "PerformanceAnalytics", "forcats", "tidyr", "magrittr", "plotly"
)

# Function to load and check  
check_packs(packages)

#######################################

###### tabela_eleicoes_vacina_2022####

#########################
##descriptive analytics##
########################

# Variables of interest 

summary_variables_df <- bolso_data %>%
  group_by(cod_mun_ibge) %>%
  summarise(
    "Percentual de votos em Jair Bolsonaro" = per_cand, 
    "Cobertura da vacina bivalente" = cob_biv,
    "População estimada (2021)" = pop_est,
    "Pib per capita (2020)" = pib_per_c_2020, 
    "Índice de Gini (2010)" = gini_2010, 
    .groups = 'drop', 
    na.rm = TRUE) %>%
  as.data.frame()

# Table 
variable_summary_table <- datasummary(All(summary_variables_df) ~ Mean + median*Arguments(na.rm=TRUE) + min*Arguments(na.rm=TRUE) + max*Arguments(na.rm=TRUE) + SD + Histogram, 
                                      summary_variables_df,
                                      output = 'kableExtra',
                                      fmt = 2)%>%  
    kable_styling(full_width = TRUE,
                position = "float_left",
                                       ) %>%
    add_header_above(c("", "Estatísticas descritivas" = 6))
               


variable_summary_table

# PRESIDENTIAL ELECTION 1ST ROUND - TOTAL VOTES
result_elections <- tabela_eleicoes_vacina_2022 %>%
  group_by(nm_cand) %>%
  dplyr::summarize(total_votos = sum(total_votos))  

# PRESIDENTIAL ELECTION 1ST ROUND - % VOTES

result_percent <- result_elections %>%
  mutate(percent = total_votos / sum(total_votos) * 100) %>%
  mutate(
    total_votos = format(total_votos, big.mark = ",", scientific = FALSE),
    percent = sprintf("%.2f%%", percent)
  )  


# PRESIDENTIAL ELECTION 1ST ROUND - GENERAL RESULTS
# NATIONAL LEVEL

table <- result_percent %>%
  rename("Candidato"= nm_cand,"Total de votos" = total_votos, "Percentual de votos" = percent)%>%
  kable("html", booktabs = TRUE, align = c("l", "r", "r"), caption = "Resultado das eleições 2022") %>%
  kable_styling("striped")
  save_kable("resultado_prbr_2022.png")

table

# Summary data - regions
s
ummary_data <- tabela_eleicoes_vacina_2022 %>%
  group_by(nm_cand, regiao) %>%
  dplyr::summarize(votos = sum(total_votos))


# Df votes/regions

pivot_data <- summary_data %>%
  group_by(nm_cand) %>%
  pivot_wider(names_from = regiao, values_from = votos)%>% 
  mutate(total = rowSums(pick(where(is.numeric),-1)))


table<- pivot_data %>%
  select(nm_cand =, everything()) %>%
  rename(Candidato = nm_cand)%>%
  formattable(align = "l", rep("r"))

print(table)


# Percents 
pivot_data[, 2:6] <- pivot_data[, 2:6] / rowSums(pivot_data[, 2:6]) * 100
pivot_data$total <- (pivot_data$total / sum(pivot_data$total)) * 100

pivot_data <- pivot_data  %>%
  mutate(
    across(where(is.numeric), ~ digits(.x, 2))
  )

tabela_resultado_pr_regioes <- pivot_data %>%
  rename("Candidato"= nm_cand,"Total" = total)%>%
  mutate(
    across(where(is.numeric), ~ round(.x, 2)))%>%
  kable("html", booktabs = TRUE, align = c("l", "r", "r", "r", "r", "r", "r"), caption = "Resultado das eleições 2022 - percentual de votos por região") %>%
  kable_styling("striped")

# Exporting
save_kable("data/tabela_resultado_pr_regioes.png")

tabela_resultado_pr_regioes

modelsummary(pivot_data , output = "table.docx")


########################

###########
#BOLSONARO#
###########

########################

# Bolsonaro's vote share map

Mapa_bolso_votos <- ggplot(bolso_data) +
  geom_sf(mapping = aes(fill = per_cand), 
          show.legend = TRUE,
          linewidth = 0.1,
  ) +
  coord_sf() +
  aes(geometry = geometry) +
  # color palette 
  scale_fill_gradient(
    high = "red",
    low = "yellow",
    guide = "colourbar",
    limits = c(0, 100), breaks = c(0, 25, 50, 75, 100),
    name = "% Votos em Jair Bolsonaro"
  ) +
  ggtitle("Percentual de votos em Jair Bolsonaro na eleição de 2022") +
  theme_tufte() +  
  theme(
    legend.position = "bottom",
    legend.title = element_text(family = "Arial", size = 11),
    plot.title = element_text(family = "Arial", size = 11)    
  )


Mapa_bolso_votos

ggsave("TCC_USP_FINAL/imagens_tabelas/Mapa_bolso_votos.png")

# Bolsonaro model

bolso_data <- filter(tabela_eleicoes_vacina_2022, nm_cand == "Jair Bolsonaro (PL)")  

# Regression models #

# Correlation matrix

model <- lmer(cob_biv ~ per_cand + log_pop + pib_per_c_2020 + gini_2010 + (1 | uf), data = bolso_data)

# Variance-covariance matrix 
vc_matrix <- vcov(model)

# Variance-covariance matrix -> correlation matrix
cor_matrix <- cov2cor(vc_matrix)

# Alternative load of data for pratical reasons
cor_matrix <- matrix(c(1, -0.276, -0.339, 0.026, -0.561, 
                       -0.276, 1, -0.249, -0.154, 0.216, 
                       -0.339, -0.249, 1, -0.05, -0.309, 
                       0.026, -0.154, -0.05, 1, -0.026, 
                       -0.561, 0.216, -0.309, -0.026, 1), 
                     nrow = 5)

rownames(cor_matrix) <- c("(Intercepto)", "% Voto bolsonarista", "População (log)", "Pib per capita 2020", "Gini 2010")
colnames(cor_matrix) <- c("(Intercepto)", "% Voto bolsonarista", "População (log)", "Pib per capita 2020", "Gini 2010")

cor_matrix %>%
  kbl(caption = "Matriz de correlação entre variáveis") %>%
  kable_classic(full_width = F, html_font = "Arial")

# Alternative Output as correlogram

corrplot(cor_matrix, method = "number", cl.pos = "b")  

# Mixed effects regression model 

models_bolso <- list(
  "Modelo 1" = lmer(cob_biv ~ per_cand + (1|uf), data = bolso_data),
  "Modelo 2" = lmer(cob_biv ~ per_cand + log_pop + (1|uf), data = bolso_data),
  "Modelo 3" = lmer(cob_biv ~ per_cand + log_pop + pib_per_c_2020 + (1|uf), data = bolso_data),
  "Modelo 4" = lmer(cob_biv ~ per_cand + log_pop + pib_per_c_2020 + gini_2010 + (1|uf), data = bolso_data)
)

coef_map <- list(
  "(Intercept)" = "Intercepto",
  "per_cand" = "Percentual de voto Bolsonaro",
  "log_pop" = "População(log)",
  "pib_per_c_2020" = "Pib per capita",
  "gini_2010" = "Índice de Gini",
  "SD (Intercept uf)" = "SD uf",
  "SD (Observations)" = "SD obs",
   title = "Modelos hierárquicos"
   ) 

modelsummary(models_bolso,
            estimate = "estimate",
            statistic = "std.error", "effect", 
            stars = TRUE,
            title = "Modelos hierárquicos",
            coef_map = coef_map,
            gof_omit = 'Adj',
            output = "markdown"
            )

modelsummary(models_bolso, output = "markdown")

# Extract equation

eq_model <- lmer(cob_biv ~ per_cand + log_pop + pib_per_c_2020 + gini_2010 + (1|uf), data = bolso_data)

extract_eq(eq_model)

# Vote density plot for regions 
# Bolsonaro 
#

density_bolso_regiao <- ggplot(data = bolso_data, aes(x = per_cand, group = regiao, fill = regiao)) +
  geom_density(adjust = 1.5, alpha = 0.4) +
  guides(fill = guide_legend(title = "Regiões")) +
  labs(title = "Distribuição de votos de Jair Bolsonaro por região - percentual") + 
  labs(x = "Percentual de votos", y = "Density") +
  theme_tufte() +   
  theme(
    legend.position = "bottom",  
    legend.title = element_text(family = "Arial", size = 11),  
    legend.text = element_text(family = "Arial", size = 11),  
    legend.justification = "center",   
    panel.spacing = unit(0.1, "lines")
  )

density_bolso_regiao

ggsave("data/density_bolso_regiao.png")

# Same, but different

plot_bolsonaro_vacina_regiao <- ggplot(bolso_data, aes(x=per_cand, weight = cob_biv, color=regiao, fill = regiao)) +
  geom_histogram(binwidth=5) +
  ylab("Taxa de vacinação bivalente") +
  xlab("Percentual de votos em Jair Bolsonaro")+
  geom_rug(sides="f", length = unit(0.2, "native"))+
  scale_y_continuous(expand = c(0.1, 0.1))+
  scale_x_continuous(expand = c(0.1, 0.1))+
  ggtitle("Percentual de votos em Jair Bolsonaro na eleição de 2022") +
  theme_tufte() +  
  theme(
    legend.position = "bottom",
    legend.title = element_text(family = "Arial", size = 11),
    plot.title = element_text(family = "Arial", size = 11)    
  )

plot_bolsonaro_vacina_regiao <- ggplotly(plot_bolsonaro_vacina_regiao)

plot_bolsonaro_vacina_regiao

########################

###########
#LULA#
###########

########################

# Lula's votes

lula_data <- tabela_eleicoes_vacina_2022 %>%
  filter(nm_cand == "Lula (PT)")  

# Lula's vote share map

Mapa_lula_votos <- ggplot(lula_data) +
  geom_sf(mapping = aes(fill = per_cand), 
          show.legend = TRUE,
          linewidth = 0.1,
  ) +
  coord_sf() +
  aes(geometry = geometry) +
  # color palette 
  scale_fill_gradient(
    high = "red",
    low = "yellow",
    guide = "colourbar",
    limits = c(0, 100), breaks = c(0, 25, 50, 75, 100),
    space = "Lab",
    name = "% Votos em Luis Inácio Lula da Silva"
  ) +
  ggtitle("Percentual de votos em Luis Inácio Lula da Silva na eleição de 2022") +
  theme_tufte() + 
  theme(
    legend.position = "bottom",
    legend.title = element_text(family = "Arial", size = 11),  
    plot.title = element_text(family = "Arial", size = 11) 
  )

Mapa_lula_votos

ggsave("data/Mapa_lula_votos.png")

#Vote density plot for regions 
# Lula

density_lula_regiao <- ggplot(data = lula_data, aes(x = per_cand, group = regiao, fill = regiao)) +
  geom_density(adjust = 1.5, alpha = 0.4) +
  guides(fill = guide_legend(title = "Regiões")) +
  labs(title = "Distribuição de votos de Luis Inácio Lula da Silva por região - percentual") + 
  labs(x = "Percentual de votos", y = "Density") +
  theme_tufte() +  
  theme(
    legend.position = "bottom",  
    legend.title = element_text(family = "Arial", size = 11),   
    legend.text = element_text(family = "Arial", size = 11),   
    legend.justification = "center",   
    panel.spacing = unit(0.1, "lines")
  )

density_lula_regiao

ggsave("TCC_USP_FINAL/imagens_tabelas/density_lula_regiao.png")

# Regression models #

# Correlation matrix

model_lula <- lmer(cob_biv ~ per_cand + log_pop + pib_per_c_2020 + gini_2010 + (1 | uf), data = lula_data)

# Variance-covariance matrix 
vc_matrix <- vcov(model)

# Variance-covariance matrix -> correlation matrix
cor_matrix <- cov2cor(vc_matrix)


# Matrix
cor_matrix <- matrix(c(1, -0.4138611, -0.5042725, -0.08017363, -0.38293982, 
                       -0.4138611, 1, 0.2967064, 0.15607411, -0.22249704, 
                       -0.5042725, 0.2967064, 1, -0.040389, -0.31762192, 
                       -0.08017363, 0.15607411, -0.040389, 1, -0.02752101, 
                       -0.38293982, -0.22249704, -0.31762192, -0.02752101, 1), 
                     nrow = 5) 


# 
rownames(cor_matrix) <- c("(Intercepto)", "% Voto lulista", "População (log)", "Pib per capita 2020", "Gini 2010")
colnames(cor_matrix) <- c("(Intercepto)", "% Voto lulista", "População (log)", "Pib per capita 2020", "Gini 2010")

print(cor_matrix)


cor_matrix %>%
  knitr::kable(caption = "Matriz de correlação entre variáveis",
               "html") %>%
  kable_classic(full_width = F, html_font = "arial")

# Alternative Output as correlogram

corrplot(cor_matrix, method = "number", cl.pos = "b")  

# Mixed effects regression model 

models_lula <- list(
  "Modelo 1" = lmer(cob_biv ~ per_cand + (1|uf), data = lula_data),
  "Modelo 2" = lmer(cob_biv ~ per_cand + log_pop + (1|uf), data = lula_data),
  "Modelo 3" = lmer(cob_biv ~ per_cand + log_pop + pib_per_c_2020 + (1|uf), data = lula_data),
  "Modelo 4" = lmer(cob_biv ~ per_cand + log_pop + pib_per_c_2020 + gini_2010 + (1|uf), data = lula_data)
)

coef_map <- list(
  "(Intercept)" = "Intercepto",
  "per_cand" = "Percentual de voto Lula",
  "log_pop" = "População(log)",
  "pib_per_c_2020" = "Pib per capita",
  "gini_2010" = "Índice de Gini",
  "SD (Intercept uf)" = "SD uf",
  "SD (Observations)" = "SD obs",
  title = "Modelos hierárquicos"
) 


modelsummary(models_lula,
             estimate = "estimate",
             statistic = "std.error", "effect", 
             stars = TRUE,
             title = "Modelos hierárquicos",
             coef_map = coef_map,
             gof_omit = 'Adj',
             output = "markdown"
)


modelsummary(models_lula)

###########
# VACCINES#
###########

# Maps

mapa_bivalente <- ggplot(xxxx) +
  geom_sf(mapping = aes(fill = cob_mun_res), 
          show.legend = TRUE,
          linewidth = 0.1,
  ) +
  coord_sf() +
  aes(geometry = geometry) +
  # color palette 
  scale_fill_viridis(option = "B",
                     direction = -1,
                     name = "Percentual de residentes vacinados") +
  ggtitle("Proporção residente da população vacinada com vacina bivalente - até setembro de 2023") +
  guides(colour = "guide_colourbar") +
  theme_tufte() +  
  theme(
    legend.position = "bottom",  
    legend.title = element_text(family = "Arial", size = 11),   
    plot.title = element_text(family = "Arial", size = 11)   
  )

mapa_bivalente

ggsave("data/mapa_bivalente.png")


# facet_wrap vaccine coverage/region  - percent

biv_regioes <- ggplot(data = xxxx, aes(x = cob_mun_res, group = regiao, fill = regiao)) +
  geom_density(adjust = 1.5, alpha = 0.4) +
  guides(fill = guide_legend(title = "Regiões")) +
  labs(
    title = "Cobertura vacinal de residentes por região - percentual",
    x = "Percentual de residentes vacinados com a vacina bivalente até setembro de 2023",
    y = "Density"
  ) +
  facet_wrap(~regiao) +
  theme_tufte() +
  theme(
    legend.position = "bottom",   
    legend.title = element_text(family = "Arial", size = 11),  
    legend.text = element_text(family = "Arial", size = 11),   
    panel.spacing = unit(0.1, "lines")
  )

biv_regioes

ggsave("Tdata/biv_regioes.png")

# 

summary_covid_bivalente_regiao <- tabela_eleicoes_vacina_2022 %>%
  group_by(regiao) %>%
  summarise(
    "Cobertura média"  = mean(cob_biv),
    "Cobertura mediana" = median(cob_biv),
    "Desvio padrão" = sd(cob_biv),
    "Cobertura mínima" = min(cob_biv),
    "Cobertura máxima" = max(cob_biv)
  )

# Convert to % using formattable package
summary_covid_bivalente_regiao_formated <- summary_covid_bivalente_regiao %>%
  mutate(across(starts_with("cob"), ~percent(. *1, format = "f", digits = 3)))

summary_covid_bivalente_regiao_formated %>%
  knitr::kable(caption = "Cobertura vacinal da vacina bivalente por região",
               "html") %>%
  kable_classic(full_width = F, html_font = "arial")


print(summary_covid_bivalente_regiao_formated)

write_csv(summary_covid_bivalente_regiao_formated, "summary_covid_bivalente_regiao.csv")

#

voto_biv_wrap <- ggplot(data = tabela_eleicoes_vacina_2022, aes(x = per_cand, y = cob_biv*100, color = nm_cand)) +
  geom_point() +
  labs(
    title = "Votos e cobertura vacinal da vacina bivalente nos municípios",
    x = "Percentual de votos do candidato",
    y = "Percentual de cobertura bivalente"
  ) +
  facet_wrap(~nm_cand) +
  theme_minimal() +  
  theme(
    legend.position = "none",  
    legend.title = element_text(family = "Arial", size = 11),  
    legend.text = element_text(family = "Arial", size = 11),   
    panel.grid.major = element_blank(),   
    panel.grid.minor = element_blank(),   
    panel.spacing = unit(0.1, "lines")
  )

voto_biv_wrap

ggsave("data/voto_biv_wrap.png")

#

KDE_bivalente <- ggplotly(
  ggplot(tabela_eleicoes_vacina_2022, aes(x = cob_biv)) +
    geom_density(aes(x = cob_biv), 
                 position = "identity", color = "black", linewidth = 1) +
    geom_histogram(aes(y = after_stat(density)), color = "white", fill = "deepskyblue",
                   bins = 30) +
    ggtitle("Kernel Density Estimation (KDE) - função densidade de probabilidade da
variável dependente (Taxa de vacinação da bivalente nos municípios)") +
    theme_tufte() +  
    theme(
      legend.position = "bottom",
      legend.title = element_text(family = "Arial", size = 11),
      plot.title = element_text(family = "Arial", size = 11)    
    ))

KDE_bivalente 

ggsave("data/KDE_bivalente.png")
